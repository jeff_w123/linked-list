const nodeLists = [
    ['a', 'b'],
    ['r', 's'],
    ['b', 'c'],
    ['x', 'c'],
    ['q', 'r'],
    ['y', 'x'],
    ['w', 'z']
];

const testInputs = [
    ['a', 'q', 'w'],
    ['a', 'c', 'r'],
    ['y', 'z', 'a', 'r'],
    ['a', 'w']
];

const DoLinkedListIntersect = () => testInputs.map(j => {

    // find and store all the list indexes used by this input set j (list)
    const nodeListIndexesUsed = [];

    // Bool result with default for this input set
    let boolResult = false;

    // For each node in the input
    for (const [idxTestInput, k] of j.entries()) {

        nodeLists.forEach((i, idxNodeList) => {

            // Check only once, on the first iteration of nodeLists.
            // If found circular data, then exit (requirement)
            if(!idxTestInput){
                const mergedUnique = [...new Set(nodeLists[idxNodeList])];
                if(mergedUnique.length < nodeLists[idxNodeList].length) 
                throw new Error(`Error: There is at least one duplicate node in list number ${idxNodeList + 1}. Program exiting...`)
            }
            
            const filteredList = i.filter(j => j === k);
    
            // If found and index not in list, then add to list
            if(filteredList[0] && !nodeListIndexesUsed.includes(idxNodeList)) nodeListIndexesUsed.push(idxNodeList);
        });    

        // Merge input lists, then filter by unique.
        const mergedLists = nodeListIndexesUsed.map(idx => nodeLists[idx]).flat();
        const mergedUnique = [...new Set(mergedLists)];

        // If at least one duplicate exists, set bool true and break.
        if(mergedUnique.length < mergedLists.length){ boolResult = true; break; };
    }
    return boolResult;
});

// Run
console.log(DoLinkedListIntersect());

// Output
//
// [ false, true, true, false ] 