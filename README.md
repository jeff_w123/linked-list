# Linked List Intersection Question


## Overview

You are given a collection of singly-linked lists. Return true of any of them that share a common node, or false otherwise. Assume the linked lists might be large in size. Stop traversing and return immediately if you detect a common node (i.e. not depth first search (DFS) style). If a cycle is detected, please throw an error.

Implement this function like so: DoLinkedListIntersect(Collection<SinglyLinkedList>) returns bool

## Input

Your program should read lines of text from standard input or a text file. The first lines of the input will describe the singly-linked-lists in a graph format (DAG). The graph description language is a similar idea to the GraphViz graph description language, see: https://en.wikipedia.org/wiki/DOT_(graph_description_language). Each node is described as a string token, which is a unique identifier for the node. So "a->b" means a DAG with node "a" connected to node "b". As we are describing singly-linked-lists, take it as a given that the out degree of every node is either zero or one.

After each of the edges has been described, then each subsequent line will include set of SLL starting nodes to traverse from.

## Example input

a->b

r->s

b->c

x->c

q->r

y->x

w->z


a,q,w

a,c,r

y,z,a,r

a,w

## Output
For each SLL print true or false based on whether any of the traversals intersect, which is to say, whether they share a common node.

## Example output for the above input

False
True
True
False
